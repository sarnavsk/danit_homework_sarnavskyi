import { Component } from "react";
import PropTypes from 'prop-types';

class Header extends Component {
  constructor() {
    super();
  }

  render() {
    const { productsCart, productsFavourite } = this.props;
    return (
      <div className="header">
        <h3 className="header__name">VladetecStore</h3>
        <div className="header__basket basket-list">
          <img className="basket-list__icon" src={"./shopping-cart.svg"} />
          <span className="basket-list__number">{productsCart}</span>
        </div>
        <div className="header__favourite favourite-list">
          <img className="favourite-list__icon" src={"./favourite.svg"} />
          <span className="favourite-list__number">{productsFavourite}</span>
        </div>
      </div>
    );
  }
}

Header.propTypes = {
  productsCart: PropTypes.number,
  productsFavourite: PropTypes.number,
}

export default Header;
