import { Component } from "react";
import "./Modal.jsx";
import ReactDOM from "react-dom";

class Modal extends Component {

  render() {
    const { header, closeButton, text, actions } = this.props;
    return (
      <div className="modal">
        <div className="modal__header header">
          <h2 className="header__title">{header}</h2>
          {closeButton && <button className="header__button">X</button>}
        </div>
        <div className="modal__main main">
          <p className="main__text">{text}</p>
          <div>{actions}</div>
        </div>
      </div>
    );
  }
}

export default Modal;
