import { createStore, combineReducers, compose, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import productsListReducer from "./products/reducer";
import favouritesListReducer from "./favourite/reducer";
import cartListReducer from "./cart/reducer";

const syncFavouriteListLS = (store) => {
  return function (next) {
    return function (action) {
      if (
        action.type === "ADD_FAVOURITE" ||
        action.type === "REMOVE_FAVOURITE"
      ) {
        const result = next(action); // starts reducer
        localStorage.setItem(
          "myFavorites",
          JSON.stringify(store.getState().favouriteList)
        );
        return result;
      }
      return next(action);
    };
  };
};

const syncCartListLS = (store) => {
  return function (next) {
    return function (action) {
      if (
        action.type === "ADD_PRODUCT_IN_CART" ||
        action.type === "REMOVE_PRODUCT_FROM_CART" ||
        action.type === "REMOVE_ALL_PRODUCT_FROM_CART"
      ) {
        const result = next(action); // starts reducer
        localStorage.setItem(
          "myCart",
          JSON.stringify(store.getState().cart.cartList)
        );
        return result;
      }
      return next(action);
    };
  };
};

const getFavoritesListFromLS = () => {
  try {
    const favorites = localStorage.getItem("myFavorites");
    if (!favorites) return [];
    const parsed = JSON.parse(favorites);
    return parsed;
  } catch (e) {
    return [];
  }
};

const getCartListFromLS = () => {
  try {
    const cart = localStorage.getItem("myCart");
    if (!cart) return [];
    const parsed = JSON.parse(cart);
    return parsed;
  } catch (e) {
    return [];
  }
};

const initialState = {
  modalIsOpen: false,
  product: { productList: [], isLoading: false, hasError: false },
  favouriteList: getFavoritesListFromLS(),
  cart: {
    cartList: getCartListFromLS(),
    modalIsOpen: false,
    buyModalIsOpen: false,
  },
};

export const reducer = combineReducers({
  product: productsListReducer,
  favouriteList: favouritesListReducer,
  cart: cartListReducer,
});

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__
  ? window.__REDUX_DEVTOOLS_EXTENSION__()
  : (f) => f;

const store = createStore(
  reducer,
  initialState,
  compose(applyMiddleware(thunk, syncFavouriteListLS, syncCartListLS), devTools)
);

export default store;
