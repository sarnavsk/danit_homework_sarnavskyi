import Card from "../Card/Card"
import PropTypes from "prop-types"
import { useSelector } from "react-redux"
import CartCard from "../CartCard/CartCard"
import React from "react"
import { ViewContext } from "../../ViewContext"

const CardsList = () => {
  const value = React.useContext(ViewContext)
  const productList = useSelector((state) => state.product.productList)
  const isLoading = useSelector((state) => state.product.isLoading)
  const hasError = useSelector((state) => state.product.hasError)

  return (
    <div className={!value ? "cardlist-container" : "cartcard-box"}>
      {hasError ? (
        <span>Opps, error! Please, try again!</span>
      ) : isLoading ? (
        <span className="cardlist-container__loading">Loading...</span>
      ) : value ? (
        productList.map((card) => {
          return (
            <CartCard
              key={card.vendorCode}
              my_key={card.vendorCode}
              myKeyButton={card.vendorCode}
              name={card.name}
              price={card.price}
              color={card.color}
              imagePath={card.imagePath}
            />
          )
        })
      ) : (
        productList.map((card) => {
          return (
            <Card
              key={card.vendorCode}
              my_key={card.vendorCode}
              myKeyButton={card.vendorCode}
              name={card.name}
              price={card.price}
              color={card.color}
              imagePath={card.imagePath}
            />
          )
        })
      )}
    </div>
  )
}

export default CardsList

CardsList.propTypes = {
  productList: PropTypes.array,
}
