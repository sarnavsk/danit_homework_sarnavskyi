import "./Modal.jsx"
import PropTypes from "prop-types"
import { useDispatch } from "react-redux"
import { hideModalCart } from "../../store/cart/actions.js"

const Modal = () => {
  const dispatch = useDispatch()

  const hideModal = () => {
    dispatch(hideModalCart(false))
  }

  return (
    <div className="modal">
      <div className="modal__header header">
        <button onClick={hideModal} className="header__button">
          X
        </button>
      </div>
      <div className="modal__main main">
        <h3 className="main__text">
          Your products have been added to the cart
        </h3>
        <div>
          <button onClick={hideModal} className="main__button">
            Ok
          </button>
        </div>
      </div>
    </div>
  )
}

export default Modal

Modal.propTypes = {
  updateData: PropTypes.func,
}
