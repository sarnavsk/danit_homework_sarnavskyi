import { useState, useEffect } from "react";
import { Routes, Route } from "react-router-dom";
import "./App.scss";
import getProductList from "./api/getProductList";
import CardsList from "./components/CardsList/CardsList";
import Header from "./components/Header/Header";
import Modal from "./components/Modal/Modal";
import Cart from "./pages/Cart/Cart";
import Favourite from "./pages/Favourite/Favourite";
import Error from "./pages/Error/Error";

const App = () => {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [productList, setProductList] = useState([]);
  const [favouriteList, setFavouriteList] = useState([]);
  const [cartList, setCartList] = useState([]);

  useEffect(() => {
    getProductList().then((data) => {
      setProductList(data);
    });
    let cartListLocalStorage = JSON.parse(localStorage.getItem("cartList"));
    let favouriteListLocalStorage = JSON.parse(localStorage.getItem("favouriteList"));
    if (cartListLocalStorage) {
      setCartList([...cartList, ...cartListLocalStorage]);
    }
    if (favouriteListLocalStorage) {
      setFavouriteList([...favouriteList, ...favouriteListLocalStorage]);
    }
  }, []);

  const buttonBuyClick = (event, key) => {
    const buyCardThereIs = cartList.some(
      (product) => product.vendorCode === key
    );
    if (!buyCardThereIs) {
      let productCart = productList.filter(
        (product) => product.vendorCode === key
      );
      setCartList([...cartList, productCart[0]]);
      setModalIsOpen(true);
      localStorage.setItem("cartList", JSON.stringify(cartList));
    }
  };

  const bodyContainerClick = (event) => {
    setModalIsOpen(false);
  };

  const clickFavourite = (event, key) => {
    const favouriteCardThereIs = favouriteList.some(
      (product) => product.vendorCode === key
    );

    if (!favouriteCardThereIs) {
      let favouriteCard = productList.filter(
        (product) => product.vendorCode === key
      );
      setFavouriteList([...favouriteList, favouriteCard[0]]);
      localStorage.setItem("favouriteList", JSON.stringify(favouriteList));
    }
  };

  const removeClickFromCart = (event, key) => {
    setCartList(cartList.filter((card) => card.vendorCode !== key));
    localStorage.setItem("cartList", JSON.stringify(cartList));
  };

  const deleteFavouriteCard = (event, key) => {
    setFavouriteList(favouriteList.filter((card) => card.vendorCode !== key));
    localStorage.setItem("favouriteList", JSON.stringify(favouriteList));
  };

  return (
    <div className={"body-container"}>
      <Header
        productsCart={cartList.length}
        productsFavourite={favouriteList.length}
      />
      <div className="modal-container">
        {modalIsOpen && <Modal updateData={bodyContainerClick} />}
      </div>

      <Routes>
        <Route
          path="/"
          element={
            <div className="app-container">
              <CardsList
                productList={productList}
                clickOpenModal={buttonBuyClick}
                clickFavourite={clickFavourite}
              />
            </div>
          }
        />
        <Route
          path="/cart"
          element={
            <Cart
              cartList={cartList}
              removeClickFromCart={removeClickFromCart}
            />
          }
        />
        <Route
          path="/favourite"
          element={
            <div className="favourite-container">
              <Favourite
                favouriteList={favouriteList}
                clickFavourite={deleteFavouriteCard}
                clickBuyFavourite={buttonBuyClick}
              />
            </div>
          }
        />
        <Route path="*" element={<Error />} />
      </Routes>
    </div>
  );
};

export default App;

App.defaultProps = {
  modalIsOpen: false,
};
