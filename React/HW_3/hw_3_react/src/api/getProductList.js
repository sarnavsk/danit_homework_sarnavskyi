const getProductList = () => {
  return fetch("/productList.json",{
    headers : { 
      'Content-Type': 'application/json',
      'Accept': 'application/json'
     }
  }).then(result=>result.json());
};

export default getProductList;
