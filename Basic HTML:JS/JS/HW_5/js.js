//Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования?
//Это косая черта \, которая используется для обозначения классов символов.


let userFirstName = prompt('Введите ваше имя:');
while (isValidNumber(userFirstName)) {
    userFirstName = prompt('Ещё раз введите ваше имя:');
}

let userSecondName = prompt('Введите вашу фамилию:');
while (isValidNumber(userSecondName)) {
    userSecondName = prompt('Ещё раз введите вашу фамилию:');
}

function isValidNumber(value) {
    if (value === "" || value === null || value.match(/\d/g)) return true;
}

function createNewUser(firstName, lastName) {

    let date = new Date();
    let birthDay = prompt('Введите ваш день рождения (текст в формате dd.mm.yyyy):');

    return {
        firstName,
        lastName,
        birthDay,

        getLogin() {
            let sliceName = this.firstName.slice(0, 1);
            return sliceName.toLowerCase() + this.lastName.toLowerCase();
        },

        getPassword() {
            let sliceName = this.firstName.slice(0, 1);
            let sliceBirthDay = this.birthDay.slice(6, 10);
            return sliceName.toUpperCase() + this.lastName.toLowerCase() + sliceBirthDay;
        },

        getAge() {
            return date.getFullYear() - this.birthDay.slice(6, 10);
        }
    }
}

let newUser = createNewUser(userFirstName, userSecondName);
console.log('Результат метода getLogin: ' + newUser.getLogin());
console.log('Результат метода getPassword(): ' + newUser.getPassword());
console.log('Ваш возраст: ' + newUser.getAge());
console.log(newUser);
