document.querySelector('.tabs').addEventListener('click', function (event) {
    if(event.target.tagName === 'LI'){
        if (document.querySelector('.actives')) {
            document.querySelector('.actives').classList.remove('actives');
            event.target.classList.add('actives');
            event.preventDefault();
        }
        document.querySelectorAll(`.tabs-content-item`).forEach(element=> {element.classList.remove('active')})
        document.querySelector(`.tabs-content li[data-tab="${event.target.dataset.tab}"]`).classList.add('active');
    }
})

