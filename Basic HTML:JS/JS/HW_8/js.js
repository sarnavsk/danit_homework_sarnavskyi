//Опишите своими словами, как Вы понимаете, что такое обработчик событий.
//это функция, которая сработает когда будет выполнена определенное действие

const body = document.querySelector('.list-item');

const isValidation = (element) => {
    return !(element <= 0 || isNaN(element));
}

const showValidResult = (element) => {
    const divContainer = document.querySelector('.div-container')
    divContainer.insertAdjacentHTML('beforebegin', `<span class='span-item'><span>Текущая цена: ${element}</span><span onclick='removeItem(this)' class='delete-btn' >x</span></span>`);
    document.getElementById("error").remove();
};

const showInValidResult = () => {
    let result = document.getElementById("error");
    if (result) {
        result.textContent = "Please enter correct price";
    } else {
        let newElement = document.createElement("p");
        newElement.id = "error";
        newElement.classList.add("invalid-elem");
        newElement.textContent = "Please enter correct price";
        newElement.style.cssText = 'color: red; font-weight: bold; font-size: 25px; margin-left: 5px';
        document.getElementById("input-zone").after(newElement)

    }
};

body.addEventListener('click', function () {
    const inputValue = Number(document.getElementById("input-zone").value);
    if (isValidation(inputValue)) {
        showValidResult(inputValue);
    } else {
        showInValidResult();
    }
})

const removeItem = (element) => {
    let isAgree = confirm('Удалить элемент?');
    if (isAgree) element.parentNode.remove();
}







