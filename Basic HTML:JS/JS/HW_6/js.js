/*
let numberArray = prompt('Введите количество элементов массива: ');
var arr = []; // создаем массив
for (var i = 0; i < numberArray; i++) {
    (arr[i] = prompt('Введите что хотите в массив №' + i))
}
console.log(arr); // Выводим массив целиком

let userTypeOf = prompt('Ваш тип данных для фильтрации массива');

function filterBy(arr, type) {

        for (let i = 0; i < arr.length; i++) {
             if (typeof arr[i] === type) return;

         }
}
*/

// Получить от пользователя данные
// о нужной конфигурации автомобиля: марка, цвет, год выпуска, пробег.
// Год выпуска проверить на число и на соответствие диапазону 1960 и текущим годом.
// Пробег проверить на число и на то что число больше 0 и меньше 200000.
// Из полученных данных создать объект автомобиля с помощью функции.
// Функция принимаем в себя параметры автомобиля и возвращает объект автомобиля.
// Без переданных данных, возвращает объект вида
// {
//     brand: 'Audi',
//     appearance: {
//         color: 'white'
//     },
//     year: ${текущий год},
//     mileage: 0
// }, а с переданными - соответствующие поля заполняются нужными данными.

// Полученный объект автомобиля вывести в консоль.
// Пользователю в модальном окне вывести фразу
// `Спасибо, инфа о вашем автомобиль марки ${введеная марка} сохранена.
// Код доступа к информации ${код доступа}`.
// Код доступа сформировать из введеной марки,
// в которой все символы будут в верхнем регистре, введеного пробега и
// строки вида `13032021` - сегодняшняя дата и год.

//
// const getYear = () => {
//     let year = prompt('Введите год машины')
//     const currentYear = new Date().getFullYear();
//
//     while ((year < 1960 || year > currentYear) && year !== null && year !== '') {
//         year = prompt('Введите год машины ещё раз')
//     }
//
//     if (!year) {
//         return currentYear;
//     }
//
//     return year;
// }
//
// const getBrand = () => {
//     let brand = prompt('Введите бренд машины')
//     if (!brand) return 'Audi';
//     return brand;
// }
//
// const getColor = () => {
//     let color = prompt('Введите цвет ')
//     return color ? color : 'white';
// }
//
// const getMileage = () => {
//     let mileage = prompt('Введите цвет ')
//     return mileage ? mileage : 0;
// }
//
//
// const getUserData = () => {
//     return {
//         brand: getBrand(),
//         color: getColor(),
//         year: getYear(),
//         mileage: getMileage(),
//     }
// }
//
// function Car(carData) {
//     this.brand = carData.brand;
//     this.appereance = {color: carData.color};
//     this.year = carData.year;
//     this.mileage = carData.mileage;
// }
//
// const getPassword = (brand, mileage) => {
//     const currentDate = new Date();
//     const currentYear = currentDate.getFullYear();
//     const currentMonth = currentDate.getMonth() + 1;
//     const currentDay = currentDate.getDate();
//     return `${brand.toUpperCase()}${mileage}${currentYear}${currentMonth}${currentDay}`;
// }
//
//
// const userCar = new Car(getUserData);
// console.log(userCar);
//
// alert(`Спасибо, инфа о вашем автомобиль марки ${userCar.brand} сохранена.
// // Код доступа к информации ${getPassword(userCar.brand, userCar.mileage)}`)
//


/**
 * Задание.
 *
 * Написать калькулятор на функциях.
 *
 * Программа должна выполнять четыре математические операции:
 * - Сложение (add);
 * - Вычитание (subtract);
 * - Умножение (multiply);
 * - Деление (divide).
 *
 * Каждую математическую операцию должна выполнять отдельная функция.
 * Каждая такая функция обладает двумя параметрами-операндами,
 * и возвращает результат нужной операции над ними.
 *
 * Эти вспомогательный функции использует главная функция calculate,
 * которая обладает тремя параметрами:
 * - Первый — числовой тип, первый операнд;
 * - Второй — числовой тип, второй операнд;
 * - Третий — функцию, ссылка на ранее созданную вспомогательную функцию.
 *
 * Условия:
 * - Никаких проверок типов данных совершать не нужно;
 * - Обязательно использовать паттерн «callback».
 */

// const add = (num1, num2) => num1 + num2;
//
// const subtract = (num1, num2) => num1 - num2;
//
// const multiply = (num1, num2) => num1 * num2;
//
// const divide = (num1, num2) => num1 / num2;
//
//
// const calculate = (num_1, num_2, callback) => callback(num_1, num_2)
//
// console.log(calculate(5, 6, add))







// Требуется написать функцию, выводящую в консоль числа от 1 до n, где n — это целое число, которая функция принимает в качестве параметра, с такими условиями:
// вывод fizz вместо чисел, кратных 3;
// вывод buzz вместо чисел, кратных 5;
// вывод fizzbuzz вместо чисел, кратных как 3, так и 5.



//
// let displayNumber = (number) => {
//     for (let i=1; i <= number; i++ ){
//         if(i % 3 === 0 && i % 5 === 0)
//         {
//             console.log('fizzbuzz');
//         }else if(i % 3 === 0) {
//             console.log('fizz');
//         }else if(i % 5 === 0){
//             console.log('buzz');
//         } else{
//             console.log('i');
//         }
//     }
// }
//
// displayNumber(11)


// Напишите функцию, которая принимает цвет фона,
// цвет текста, строку с текстом и выводит методом document.write()
// абзац с соответствующими стилями. Например, функция myParagraph("red", "white", "Lorem ipsum sit amet")
// выведет абзац с текстом "Lorem ipsum sit amet" белого цвета на красном фоне.

//
// let showColorString = (bgColor, color, text) => {
//     let tag = `<p style = "background-color: ${bgColor}; color: ${color}"> ${text} </p>`
//     document.write(tag);
// }
//
// showColorString("red", "white", "Lorem ipsum sit amet");



// Напишите функцию, которая принимает строку и из строчных букв делает прописные,
// а прописные - строчными.
// example('hello world') => 'HELLO WORLD'
// example('HELLO WORLD') => 'hello world'
// example('hello WORLD') => 'HELLO world'
// example('HeLLo WoRLD') => 'hEllO wOrld'
// example('12345') => '12345'
// example('1a2b3c4d5e') => '1A2B3C4D5E'



/*let changeRegister = (string) => {
    for(let i = 0; i < string.length; i++) {
        let char = string[i];
        if(char === char.toUpperCaseg(){
            result +=char.toUpperCaseg
        }

    }
}*/


// Когда спрашиваешь у маленькой девочки,
// сколько ей лет, она постоянно отвечает по-разному.
// Давайте поможем родителям, чтобы они не путались с ответами своей дочери.
// Напишите программу, которая принимает строку, а возвращает возвраст в виде цифры.
// Первый символ в строке всегда будет цифрой.
// Пример кода:
// example('4 годика мне') => 4
// example('5 лет, или нет, или... да') => 5
