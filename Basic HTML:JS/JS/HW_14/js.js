window.addEventListener('scroll', function(){
    const scrolledProgress = window.pageYOffset;
    const windowHeight = window.innerHeight;
    if(scrolledProgress >= windowHeight){
        $('#scrollToTop').addClass('active');
    }else{
        $('#scrollToTop').removeClass('active');
    }
})


$('#scrollToTop').on('click', function(){
    window.scrollTo({
        top: 0,
        behavior: "smooth"
    });
})


$('.first-part').on('click', function(event){
    event.preventDefault();
    $('html, body').animate({
        scrollTop: $('.main-title').offset().top
    }, 1000);
});


$('.second-part').on('click', function(event){
    event.preventDefault();
    $('html, body').animate({
        scrollTop: $('.section-clients').offset().top
    }, 1000);
});


$('.third-part').on('click', function(event){
    event.preventDefault();
    $('html, body').animate({
        scrollTop: $('.grid-section').offset().top
    }, 1000);
});


$('.fourth-part').on('click', function(event){
    event.preventDefault();
    $('html, body').animate({
        scrollTop: $('.footer-information').offset().top
    }, 1000);
});


$('.main-btn').on('click', function(){
    $('.section-clients').toggle('hide');
})

