//1)Почему для работы с input не рекомендуется использовать события клавиатуры?
//Потому что существует специальное событие input, чтобы отслеживать любые изменения в поле <input>

const makeBlue = (value) => {
    let pressKey = document.getElementById(value.toLowerCase());
    pressKey.classList.add('is-pressing');
}

const checkedClass = () => {
    for (let item of document.querySelectorAll('.is-pressing')) {
        item.classList.add('black-color')
    }
}

document.addEventListener('keyup', function (event) {
    checkedClass();
    makeBlue(event.code);
})