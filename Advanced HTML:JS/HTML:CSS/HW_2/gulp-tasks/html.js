const gulp = require("gulp");
const htmlmin = require("gulp-htmlmin");

function handleHtmlFiles() {
    return gulp.src("src/index.html")
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('build'));
}
exports.html = handleHtmlFiles;