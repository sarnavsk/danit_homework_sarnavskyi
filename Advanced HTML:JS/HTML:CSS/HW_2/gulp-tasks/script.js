const gulp = require("gulp");
const uglify = require('gulp-uglify');

function minJS() {
   return gulp.src("src/script/**/*.js")
            .pipe(uglify())
            .pipe(gulp.dest('build/script'));
}

exports.js = minJS;